#!/usr/bin/bash
read -p "Place the path of the C file that you want to be compiled: " path
[[ ! -f "$path" ]] && echo "Error: $path file not found." && exit
read -p "What actions do you want to execute: compile (-c), run (-r), delete (-d): " ACTION
case $ACTION in 
    -c) gcc "$path";;
    -r) [[ ! -f a.out ]] && echo "the file was not compiled yet" || ./a.out;;
    -d) rm a.out;;
    *) echo "please chose from the given choices"
esac