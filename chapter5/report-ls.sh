#!/bin/bash
for i in ls -l
do
	echo -e "These are the files: \n$(ls -l | egrep ^-r)"
    echo "[DIR] $(pwd)"
    echo "Total regular files : $(ls -l | egrep ^-r | wc -l)"
    echo "Total directories : $(pwd | egrep -o / | wc -l)"
    echo "Total symbolic links : $(ls -l | egrep -o ^lr | wc -l)"
    echo "Total size of regular files : $(du -sh .)"
    exit 
done