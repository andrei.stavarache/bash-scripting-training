#!/bin/bash
ARG1=$1
if [ ${#ARG1} -eq 0 ]; then
    echo "Argument missing"
    exit
elif [ -f $ARG1 ]; then  
    sed -n 2~2p $ARG1 > evenfile.txt
    sed -n 1~2p $ARG1 > oddfile.txt
    echo "the even lines have been placed in evenfile.txt and the odd ones in oddfile.txt"
else
    echo "the file doese not exist"
fi