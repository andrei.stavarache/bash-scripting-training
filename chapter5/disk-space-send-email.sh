#!/bin/bash

system_space=$(df -h / | egrep -o "(\b..%)" | egrep -o "[0-9][0-9]")
while true
do
    if  [ $system_space = '91' ] && [ $system_space -ge '91' ]
    then
        echo "Carefull your memorie is more than 91% occupied"
        mail -s "Carefull your memorie is more than 91% occupied" email@address.com <<< "$(df -h)"
        exit
    fi
done