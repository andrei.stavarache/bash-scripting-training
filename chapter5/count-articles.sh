#!/bin/bash
ARG1=$1
if [ ${#ARG1} -eq 0 ]; then
    echo "Argument missing"
    exit
elif [ -f ${ARG1} ]; then 
    echo "there are $(egrep -o "(\b[tT]he\b)|(\b[aA]n\b)|(\b[aA]s\b)|(\b[aA]\b)" names.txt | wc -l) vowels in $ARG1"
else    
    echo "wrong argument"
fi