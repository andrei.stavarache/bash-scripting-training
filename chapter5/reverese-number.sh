read -p 'write the number you want to make an addition with: ' num
while [ $num -gt 0 ]
do
    # Get Remainder
    s=$(( $num % 10 ))  
      
    # Get next digit
    num=$(( $num / 10 )) 
      
    # Store previous number and
    # current digit in reverse 
    rev=$( echo ${rev}${s} ) 
done
echo $rev